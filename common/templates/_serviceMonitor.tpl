{{- define "common.ServiceMonitor" }}
  {{- template "common.utils.merge" (append . "common.ServiceMonitor.tpl") }}
{{- end }}

{{- define "common.ServiceMonitor.tpl" }}
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: {{ template "common.fullname" . }}
  {{- with .Values.serviceMonitor.extraAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
  labels:
    {{- include "common.metadata.labels" . | nindent 4 }}
    {{- with .Values.serviceMonitor.extraLabels }}
      {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  namespaceSelector:
    matchNames:
      - {{ .Release.Namespace }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ template "common.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
