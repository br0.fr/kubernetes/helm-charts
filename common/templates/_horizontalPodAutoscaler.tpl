{{- define "common.HorizontalPodAutoscaler" }}
  {{- template "common.utils.merge" (append . "common.HorizontalPodAutoscaler.tpl") }}
{{- end }}

{{- define "common.HorizontalPodAutoscaler.tpl" }}
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: {{ template "common.fullname" . }}
  labels:
    {{ include "common.metadata.labels" . | nindent 4 }}
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: {{ template "common.fullname" . }}
  {{- with .Values.autoscaling.minReplicas}}
  minReplicas: {{ . }}
  {{- end }}
  {{- with .Values.autoscaling.maxReplicas }}
  maxReplicas: {{ . }}
  {{- end }}
  metrics:
    {{- with .Values.autoscaling.targetCPUUtilizationPercentage }}
    - type: Resource
      resource:
        name: cpu
        targetAverageUtilization: {{ . }}
    {{- end }}
    {{- with .Values.autoscaling.targetMemoryUtilizationPercentage }}
    - type: Resource
      resource:
        name: memory
        targetAverageUtilization: {{ . }}
    {{- end }}
{{- end }}
