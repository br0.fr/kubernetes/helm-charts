{{- define "common.utils.merge" }}
  {{- $context := first . }}
  {{- $tpl := dict }}
  {{- range $_, $template := slice . 1 }}
    {{- $prefix := first (splitList "." $template) }}
    {{- if and (ne $prefix "common") (ne $prefix $context.Chart.Name) }}
      {{- fail (printf "Template name \"%s\" doesn’t match chart name \"%s\"" $template $context.Chart.Name)}}
    {{- end }}
    {{- $overrides := fromYaml (include $template $context) | default (dict) }}
    {{- $tpl = merge $tpl $overrides }}
  {{- end }}
  {{- toYaml $tpl }}
{{- end }}
