{{- define "common.Deployment" }}
  {{- template "common.utils.merge" (append . "common.Deployment.tpl") }}
{{- end }}

{{- define "common.Deployment.tpl" }}
apiVersion: apps/v1
kind: Deployment
metadata:
  {{- include "common.metadata" . | nindent 2 }}
spec:
  replicas: {{ .Values.deployment.replicas }}
  {{- with .Values.deployment.strategy }}
  strategy:
    {{- . | toYaml | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ template "common.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      {{- with .Values.deployment.extraPodAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app.kubernetes.io/name: {{ template "common.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        {{- with .Values.deployment.extraPodLabels }}
          {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      serviceAccountName: {{ template "common.fullname" . }}
      {{- with .Values.deployment.priorityClassName }}
      priorityClassName: {{ . }}
      {{- end }}
      {{- if .Values.deployment.podSecurityContext }}
      securityContext:
        runAsUser: {{ .Values.deployment.podSecurityContext.runAsUser }}
        runAsGroup: {{ .Values.deployment.podSecurityContext.runAsGroup }}
        fsGroup: {{ .Values.deployment.podSecurityContext.fsGroup }}
      {{- end }}
      {{- with .Values.deployment.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.deployment.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}

{{- define "common.Deployment.container" }}
  {{- template "common.utils.merge" (append . "common.Deployment.container.tpl") }}
{{- end }}

{{- define "common.Deployment.container.tpl" }}
image: "{{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.tag | default .Chart.AppVersion }}"
{{- with .Values.deployment.image.pullPolicy }}
imagePullPolicy: {{ . }}
{{- end }}
{{- with .Values.deployment.extraArgs }}
args:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.deployment.extraEnv }}
env:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.deployment.resources }}
resources:
  {{- toYaml . | nindent 2 }}
{{- end }}
securityContext:
  readOnlyRootFilesystem: true
  capabilities:
    drop:
      - ALL
{{- end }}
