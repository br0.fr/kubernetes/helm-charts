{{- define "common.DaemonSet" }}
  {{- template "common.utils.merge" (append . "common.DaemonSet.tpl") }}
{{- end }}

{{- define "common.DaemonSet.tpl" }}
apiVersion: apps/v1
kind: DaemonSet
metadata:
  {{- include "common.metadata" . | nindent 2 }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ template "common.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      {{- with .Values.daemonset.extraPodAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app.kubernetes.io/name: {{ template "common.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        {{- with .Values.daemonset.extraPodLabels }}
          {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      serviceAccountName: {{ template "common.fullname" . }}
      {{- with .Values.daemonset.priorityClassName }}
      priorityClassName: {{ . }}
      {{- end }}
      {{- if .Values.daemonset.podSecurityContext }}
      securityContext:
        runAsUser: {{ .Values.daemonset.podSecurityContext.runAsUser }}
        runAsGroup: {{ .Values.daemonset.podSecurityContext.runAsGroup }}
        fsGroup: {{ .Values.daemonset.podSecurityContext.fsGroup }}
      {{- end }}
      {{- with .Values.daemonset.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.daemonset.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.daemonset.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}

{{- define "common.DaemonSet.container" }}
  {{- template "common.utils.merge" (append . "common.DaemonSet.container.tpl") }}
{{- end }}

{{- define "common.DaemonSet.container.tpl" }}
image: "{{ .Values.daemonset.image.repository }}:{{ .Values.daemonset.image.tag | default .Chart.AppVersion }}"
{{- with .Values.daemonset.image.pullPolicy }}
imagePullPolicy: {{ . }}
{{- end }}
{{- with .Values.daemonset.extraArgs }}
args:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.daemonset.extraEnv }}
env:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.daemonset.resources }}
resources:
  {{- toYaml . | nindent 2 }}
{{- end }}
securityContext:
  readOnlyRootFilesystem: true
  capabilities:
    drop:
      - ALL
{{- end }}
