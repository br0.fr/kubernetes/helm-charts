{{- define "common.rbac.ClusterRoleBinding" }}
  {{- template "common.utils.merge" (append . "common.rbac.ClusterRoleBinding.tpl") }}
{{- end }}

{{- define "common.rbac.ClusterRoleBinding.tpl" }}
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  {{- include "common.metadata" . | nindent 2 }}
{{- end}}
