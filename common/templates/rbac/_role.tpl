{{- define "common.rbac.Role" }}
  {{- template "common.utils.merge" (append . "common.rbac.Role.tpl") }}
{{- end }}

{{- define "common.rbac.Role.tpl" }}
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  {{- include "common.metadata" . | nindent 2 }}
{{- end }}
