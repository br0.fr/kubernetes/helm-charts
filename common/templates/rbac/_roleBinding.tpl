{{- define "common.rbac.RoleBinding" }}
  {{- template "common.utils.merge" (append . "common.rbac.RoleBinding.tpl") }}
{{- end }}

{{- define "common.rbac.RoleBinding.tpl" }}
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  {{- include "common.metadata" . | nindent 2 }}
{{- end}}
