{{- define "common.rbac.ClusterRole" }}
  {{- template "common.utils.merge" (append . "common.rbac.ClusterRole.tpl") }}
{{- end }}

{{- define "common.rbac.ClusterRole.tpl" }}
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  {{- include "common.metadata" . | nindent 2 }}
{{- end }}
