{{- define "common.rbac.ServiceAccount" }}
  {{- template "common.utils.merge" (append . "common.rbac.ServiceAccount.tpl") }}
{{- end }}

{{- define "common.rbac.ServiceAccount.tpl" }}
apiVersion: v1
kind: ServiceAccount
metadata:
  {{- include "common.metadata" . | nindent 2 }}
{{- end }}
