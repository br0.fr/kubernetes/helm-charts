{{- define "common.ConfigMap" }}
  {{- template "common.utils.merge" (append . "common.ConfigMap.tpl") }}
{{- end }}

{{- define "common.ConfigMap.tpl" }}
apiVersion: v1
kind: ConfigMap
metadata:
  {{- include "common.metadata" . | nindent 2 }}
{{- end }}
