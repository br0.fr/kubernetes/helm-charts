{{- define "common.StatefulSet" }}
  {{- template "common.utils.merge" (append . "common.StatefulSet.tpl") }}
{{- end }}

{{- define "common.StatefulSet.tpl" }}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  {{- include "common.metadata" . | nindent 2 }}
spec:
  replicas: {{ .Values.statefulset.replicas }}
  serviceName: {{ template "common.fullname" . }}-headless
  {{- with .Values.statefulset.podManagementPolicy }}
  podManagementPolicy: {{ . }}
  {{- end }}
  {{- with .Values.statefulset.updateStrategy }}
  updateStrategy:
    {{- . | toYaml | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ template "common.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      {{- with .Values.statefulset.extraPodAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app.kubernetes.io/name: {{ template "common.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        {{- with .Values.statefulset.extraPodLabels }}
          {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      serviceAccountName: {{ template "common.fullname" . }}
      {{- with .Values.statefulset.priorityClassName }}
      priorityClassName: {{ . }}
      {{- end }}
      {{- if .Values.statefulset.podSecurityContext }}
      securityContext:
        runAsUser: {{ .Values.statefulset.podSecurityContext.runAsUser }}
        runAsGroup: {{ .Values.statefulset.podSecurityContext.runAsGroup }}
        fsGroup: {{ .Values.statefulset.podSecurityContext.fsGroup }}
      {{- end }}
      {{- with .Values.statefulset.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.statefulset.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.statefulset.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}

{{- define "common.StatefulSet.container" }}
  {{- template "common.utils.merge" (append . "common.StatefulSet.container.tpl") }}
{{- end }}

{{- define "common.StatefulSet.container.tpl" }}
image: "{{ .Values.statefulset.image.repository }}:{{ .Values.statefulset.image.tag | default .Chart.AppVersion }}"
{{- with .Values.statefulset.image.pullPolicy }}
imagePullPolicy: {{ . }}
{{- end }}
{{- with .Values.statefulset.extraArgs }}
args:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.statefulset.extraEnv }}
env:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.statefulset.resources }}
resources:
  {{- toYaml . | nindent 2 }}
{{- end }}
securityContext:
  readOnlyRootFilesystem: true
  capabilities:
    drop:
      - ALL
{{- end }}
