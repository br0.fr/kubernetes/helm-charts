{{- define "common.Service" }}
  {{- template "common.utils.merge" (append . "common.Service.tpl") }}
{{- end }}

{{- define "common.Service.tpl" }}
apiVersion: v1
kind: Service
metadata:
  {{- include "common.metadata" . | nindent 2 }}
spec:
  selector:
    app.kubernetes.io/name: {{ template "common.name" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "common.Service.headless" }}
  {{- template "common.utils.merge" (append (append . "common.Service.headless.tpl") "common.Service.tpl") }}
{{- end }}

{{- define "common.Service.headless.tpl" }}
metadata:
  name: {{ template "common.fullname" . }}-headless
spec:
  clusterIP: None
{{- end }}
