{{- define "common.metadata" }}
name: {{ template "common.fullname" . }}
labels:
  {{- include "common.metadata.labels" . | nindent 2 }}
{{- end }}

{{- define "common.metadata.labels" }}
app.kubernetes.io/name: {{ template "common.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ template "common.chart" . }}
{{- end }}
