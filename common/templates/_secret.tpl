{{- define "common.Secret" }}
  {{- template "common.utils.merge" (append . "common.Secret.tpl") }}
{{- end }}

{{- define "common.Secret.tpl" }}
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  {{- include "common.metadata" . | nindent 2 }}
{{- end }}

{{- define "common.Secret.environment" }}
  {{- template "common.utils.merge" (append (append . "common.Secret.environment.tpl") "common.Secret.tpl") }}
{{- end }}

{{- define "common.Secret.environment.tpl" }}
metadata:
  name: {{ template "common.fullname" . }}-environment
data:
  {{- range $key, $value := .Values.config.environment }}
  {{ $key }}: {{ $value | b64enc | quote }}
  {{- end }}
{{- end }}
