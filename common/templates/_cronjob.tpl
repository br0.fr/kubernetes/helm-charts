{{- define "common.CronJob" }}
  {{- template "common.utils.merge" (append . "common.CronJob.tpl") }}
{{- end }}

{{- define "common.CronJob.tpl" }}
apiVersion: batch/v1
kind: CronJob
metadata:
  {{- include "common.metadata" . | nindent 2 }}
spec:
  {{- with .Values.cronJob.concurrencyPolicy }}
  concurrencyPolicy: {{ . }}
  {{- end }}
  schedule: {{ .Values.cronJob.schedule | quote }}
  jobTemplate:
    spec:
      template:
        metadata:
          {{- with .Values.cronJob.extraPodAnnotations }}
          annotations:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          labels:
            app.kubernetes.io/name: {{ template "common.name" . }}
            app.kubernetes.io/instance: {{ .Release.Name }}
            {{- with .Values.cronJob.extraPodLabels }}
              {{- toYaml . | nindent 12 }}
            {{- end }}
        spec:
          serviceAccountName: {{ template "common.fullname" . }}
          {{- with .Values.cronJob.priorityClassName }}
          priorityClassName: {{ . }}
          {{- end }}
          {{- if .Values.cronJob.podSecurityContext }}
          securityContext:
            runAsUser: {{ .Values.cronJob.podSecurityContext.runAsUser }}
            runAsGroup: {{ .Values.cronJob.podSecurityContext.runAsGroup }}
            fsGroup: {{ .Values.cronJob.podSecurityContext.fsGroup }}
          {{- end }}
          {{- with .Values.cronJob.nodeSelector }}
          nodeSelector:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.cronJob.affinity }}
          affinity:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- with .Values.cronJob.tolerations }}
          tolerations:
            {{- toYaml . | nindent 12 }}
          {{- end }}
{{- end }}

{{- define "common.CronJob.container" }}
  {{- template "common.utils.merge" (append . "common.CronJob.container.tpl") }}
{{- end }}

{{- define "common.CronJob.container.tpl" }}
image: "{{ .Values.cronJob.image.repository }}:{{ .Values.cronJob.image.tag | default .Chart.AppVersion }}"
{{- with .Values.cronJob.image.pullPolicy }}
imagePullPolicy: {{ . }}
{{- end }}
{{- with .Values.cronJob.extraArgs }}
args:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.cronJob.extraEnv }}
env:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with .Values.cronJob.resources }}
resources:
  {{- toYaml . | nindent 2 }}
{{- end }}
securityContext:
  readOnlyRootFilesystem: true
  capabilities:
    drop:
      - ALL
{{- end }}
